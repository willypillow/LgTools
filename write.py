from os import path
from hashlib import md5
from zlib import compress
from functools import partial
from struct import pack, unpack

imgpath = "rootedsystem.img"
indz = "RS98712a00.dz"
outdz = "out.dz"

headerfmt = "<4x32s64s4xI16s388x"
indexs = []

def getNextTo(lst, x):
    tmp = [e for e in lst if e > x]
    return min(tmp) if tmp else 0 #

with open(indz, "rb") as dz:
    with open(outdz, "wb") as out:
        with open(imgpath, "rb") as img:
            out.write(dz.read(512))
            for header in iter(partial(dz.read, 512), b""):
                parse = unpack(headerfmt, header)
                if parse[0].split(b"\0")[0] == b"system":  
                    indexs.append(int(parse[1].split(b"_")[1].split(b".")[0]))
                dz.seek(parse[2], 1)
            print(indexs)

            base = min(indexs)
            dz.seek(512)
            for header in iter(partial(dz.read, 512), b""):
                parse = unpack(headerfmt, header)
                if parse[0].split(b"\0")[0] == b"system":
                    off = int(parse[1].split(b"_")[1].split(b".")[0])
                    nxtloc = getNextTo(indexs, off)
                    img.seek((off-base) * 512)
                    if nxtloc == 0:
                      raw = img.read((int(path.getsize(imgpath)/512)+1-off+base) * 512)
                    else:
                      raw = img.read((nxtloc-off) * 512)
                    hash = md5(raw).digest()
                    zip = compress(raw)
                    out.write(header[:104])
                    out.write(pack("<I16s", len(zip), hash))
                    out.write(header[124:])
                    out.write(zip)
                    dz.seek(parse[2], 1)
                    print(parse[1])
                else:
                    out.write(header)
                    out.write(dz.read(parse[2]))
                
