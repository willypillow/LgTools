This is a coarse utility for repacking the firmware of LG devices (only tested on H962), which I originally wrote in March 2016. Refactoring and documentation will be added when I have time.
The usual disclaimers apply, use this at your own risk.
