from functools import partial

off = 901120
imgPath = './'
binPath = './bin/'

with open(imgPath + 'rootedsystem.img', 'rb') as img:
    for i, chunk in enumerate(iter(partial(img.read, 204800 * 512), b'')):
        with open(binPath + 'system_' + str(off + i * 204800) + '.bin',
                  'wb') as bin:
            bin.write(chunk)
